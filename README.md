 Hello! 

 My name is Mihnea and this is my calculator made in C++.

It can resolve every equation with decimal numbers, integers( >0, i will update it to work with negative numbers also).

It is usable with +, - , /, * , brackets ( [], ()  ), ^ - power, # - sqrt. 

I hope it helps you! 


Steps to use it: 

1. If you use Command Line, this is the example " Project.exe [4+(5-1)]*2  ". If you use spaces between characters, the program will think that you wrote 2 equations, so it wont return any result, just an error.

2. If you use Console, you just lauch the program. You will be asked if you want to save the files to binary. Then you will get an user interface, so you can select what type of function do you want.

3. Dont forget to type again the number of the function you want to use next after getting the response of the previous one if you want to continue.

4. Have fun coding!

© 2022 Mihnea Catana ALL rights reserved

